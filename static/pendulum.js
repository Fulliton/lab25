'use strict'
export default class Pendulum {
  constructor (canvas, ctx) {
    this.ctx = ctx
    this.canvas = canvas
    this.data = {
      solid: {
        startPosition: {
          x: 170,
          y: 258
        },
        freePosition: {
          x: 130,
          y: 265
        },
        currentPosition: {
          x: 0,
          y: 0
        },
        acc: {
          x: -1,
          y: 0.2,
          dy: 0.01
        },
        radius: 10
      },
      animationID: null,
      animationStart: null,
      animationTimes: null,
      currentTime: 0,
      realCurrentTime: 0,
      periodTime: 0,
      halfPeriodTime: 0,
      countHalfPeriods: 0,
      periodCounter: 0,
      offsetY: 2,
      offsetX: 4,
      counter: 0,
      rotateSpeed: 6,
      reversed: false,
      end: false,
      dx: -0.1,
      dy: 5
    }
  }

  init (startButton, resetButton) {
    this.startButton = startButton
    this.resetButton = resetButton
    this.canvas.width = 350
    this.canvas.height = 400
    this.ctx.lineWidth = 2

    this.data.solid.currentPosition.x = this.data.solid.freePosition.x
    this.data.solid.currentPosition.y = this.data.solid.freePosition.y

    // this.startButton.disabled = true
    // this.resetButton.disabled = true

    this.startButton.onclick = () => {
      if (this.data.counter === 0) {
        this.data.animationStart = Date.now()
        this.data.solid.currentPosition.x = this.data.solid.startPosition.x
        this.data.solid.currentPosition.y = this.data.solid.startPosition.y
        this.animation()
        this.startButton.disabled = true
        this.resetButton.disabled = true
        console.log(this.data.solid)
      }
    }

    this.resetButton.onclick = () => {
      cancelAnimationFrame(this.data.animationID)
      this.clear()
      this.data.animationStart = undefined
      this.data.counter = 0
      this.data.solid.currentPosition.x = this.data.solid.freePosition.x
      this.data.solid.currentPosition.y = this.data.solid.freePosition.y
      this.drawPendulum()
      this.drawSolid()
      let endArr = this.initNextTime()

      console.log(this.data.animationTimes)
      if (endArr) {
        this.startButton.disabled = true
        this.resetButton.disabled = true
      } else {
        this.startButton.disabled = false
        this.resetButton.disabled = true
      }
    }

    this.drawPendulum()
    this.drawSolid()
  }

  clear () {
    this.ctx.save()
    this.ctx.setTransform(1, 0, 0, 1, 0, 0)
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)
    this.ctx.restore()
  }

  calculate () {
    this.data.realCurrentTime = Date.now() - this.data.animationStart

    console.log(this.data.realCurrentTime - (this.data.halfPeriodTime * this.data.countHalfPeriods))
    if (this.data.realCurrentTime - (this.data.halfPeriodTime * this.data.countHalfPeriods) > this.data.halfPeriodTime) {
      this.data.countHalfPeriods++
      this.data.solid.acc.x *= -1
    }

    if (this.data.solid.acc.x < 0) {
      if (this.data.solid.currentPosition.x > this.data.solid.freePosition.x) {
        this.data.solid.currentPosition.y += this.data.solid.acc.y
        // this.data.solid.acc.y -= this.data.solid.acc.dy
      } else if (this.data.solid.currentPosition.x < this.data.solid.freePosition.x) {
        this.data.solid.currentPosition.y -= this.data.solid.acc.y
        // this.data.solid.acc.y += this.data.solid.acc.dy
      }
    } else if (this.data.solid.acc.x > 0) {
      if (this.data.solid.currentPosition.x > this.data.solid.freePosition.x) {
        this.data.solid.currentPosition.y -= this.data.solid.acc.y
        // this.data.solid.acc.y -= this.data.solid.acc.dy
      } else if (this.data.solid.currentPosition.x < this.data.solid.freePosition.x) {
        this.data.solid.currentPosition.y += this.data.solid.acc.y
        // this.data.solid.acc.y += this.data.solid.acc.dy
      }
    } else {
      this.data.solid.acc.y = 0
    }

    this.data.solid.currentPosition.x += this.data.solid.acc.x
  }

  animation () {
    if (this.data.animationTimes != null && this.data.animationTimes !== undefined) {
      this.clear()
      this.calculate()
      this.drawPendulum()
      this.drawSolid()

      if ((this.data.currentTime * 1000) - this.data.realCurrentTime <= 0) {
        cancelAnimationFrame(this.data.animationID)
        this.data.solid.currentPosition.x = this.data.solid.freePosition.x
        this.data.solid.currentPosition.y = this.data.solid.freePosition.y
        this.clear()
        this.drawPendulum()
        this.drawSolid()
        this.resetButton.disabled = false
      } else {
        this.data.animationID = requestAnimationFrame(this.animation.bind(this))
      }
    } else {
      // eslint-disable-next-line no-throw-literal
      throw "Animation times don't initialized"
    }
  }

  drawPendulum () {
    this.ctx.strokeRect(10, 380, 260, 10)// lower beam
    this.ctx.strokeRect(15, 390, 10, 10)// left leg
    this.ctx.strokeRect(255, 390, 10, 10)// right leg

    this.ctx.strokeRect(120, 40, 20, 215)
    this.ctx.strokeRect(120, 275, 20, 55)// pre-bot rack

    this.ctx.strokeRect(115, 255, 30, 20)

    this.ctx.fillStyle = 'red'
    this.ctx.beginPath()

    this.ctx.arc(130, 265, 3, 0, Math.PI * 2, false)

    this.ctx.fill()
    this.ctx.closePath()

    this.ctx.strokeRect(90, 35, 80, 5)// topper beam

    this.ctx.strokeRect(115, 30, 30, 5)// top holder
    this.ctx.strokeRect(125, 25, 10, 5)// top holder

    this.ctx.strokeRect(80, 330, 100, 50)
    this.ctx.strokeRect(90, 340, 80, 30)

    this.ctx.beginPath()

    for (let i = 0; i < 181; i += 3) {
      if ((i / 3) % 5 === 0) {
        this.ctx.moveTo(140, 70 + i)
        this.ctx.lineTo(130, 70 + i)
      } else {
        this.ctx.moveTo(140, 70 + i)
        this.ctx.lineTo(135, 70 + i)
      }
    }

    this.ctx.stroke()
    this.ctx.closePath()

    this.ctx.fillStyle = 'black'
    this.ctx.font = 'bold 14pt Arial'

    if (!this.data.end) {
      if (Date.now() - this.data.animationStart) {
        if (Date.now() - this.data.animationStart < this.data.currentTime * 1000) {
          this.ctx.fillText((Date.now() - this.data.animationStart) / 1000 + ' с.', 95, 362)
        } else {
          this.ctx.fillText(this.data.currentTime + ' с.', 95, 362)
        }
      } else {
        this.ctx.fillText('0 с.', 95, 362)
      }
    } else {
      this.ctx.fillText(this.data.realCurrentTime + ' с.', 95, 362)
    }
  }

  drawSolid () {
    this.ctx.beginPath()

    this.ctx.moveTo(130, 35)
    this.ctx.lineTo(this.data.solid.currentPosition.x, this.data.solid.currentPosition.y)
    this.ctx.stroke()
    this.ctx.closePath()

    this.ctx.beginPath()
    this.ctx.arc(this.data.solid.currentPosition.x, this.data.solid.currentPosition.y, this.data.solid.radius, 0, Math.PI * 2, false)
    this.ctx.fillStyle = 'white'
    this.ctx.fill()
    this.ctx.arc(this.data.solid.currentPosition.x, this.data.solid.currentPosition.y, this.data.solid.radius, 0, Math.PI * 2, false)
    this.ctx.stroke()
    this.ctx.closePath()
  }

  initNextTime () {
    this.data.currentTime = this.data.animationTimes.shift()
    this.data.countHalfPeriods = 0
    this.data.periodTime = (this.data.currentTime / 10) * 1000
    this.data.halfPeriodTime = (this.data.periodTime / 2)
    this.data.solid.acc.x = -(1300 / this.data.halfPeriodTime)
    // if (this.data.animationTimes.indexOf(this.data.realCurrentTime) < this.data.animationTimes.length - 1) {
    //   this.data.currentTime = this.data.animationTimes[this.data.animationTimes.indexOf(this.data.realCurrentTime) + 1]
    //   this.data.realCurrentTime = this.data.animationTimes[this.data.animationTimes.indexOf(this.data.realCurrentTime) + 1]
    // }

    if (this.data.currentTime === undefined) {
      return true;
    }
    return false;
  }

  initAnimationTimes (times) {
    this.data.animationTimes = []
    for (let i = 0; i < times.length; i++) {
      this.data.animationTimes.push(times[i])
    }
    this.initNextTime()
  }
}
